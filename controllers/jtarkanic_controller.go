/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"time"

	helmclient "github.com/mittwald/go-helm-client"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	goacademytsystemscomv1 "gitlab.com/tarkanic909/jtarkanic-operator/api/v1"
)

// JtarkanicReconciler reconciles a Jtarkanic object
type JtarkanicReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

var myFinalizerName = "batch.tutorial.kubebuilder.io/finalizer"

//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=jtarkanics,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=jtarkanics/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=jtarkanics/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Jtarkanic object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *JtarkanicReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	// TODO(user): your logic here
	var cr goacademytsystemscomv1.Jtarkanic
	var depl v1.Deployment
	err := r.Client.Get(ctx, req.NamespacedName, &cr)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	patch := client.MergeFrom(cr.DeepCopy())

	opt := &helmclient.Options{
		Namespace: cr.Spec.Namespace, // Change this to the namespace you wish the client to operate in.
		Debug:     true,
		Linting:   false,
		DebugLog:  func(format string, v ...interface{}) {},
	}

	helmClient, err := helmclient.New(opt)
	if err != nil {
		return ctrl.Result{}, err
	}

	chartSpec := helmclient.ChartSpec{
		ReleaseName:     "jtarkanic-operator",
		ChartName:       cr.Spec.HelmchartPath,
		Namespace:       cr.Spec.Namespace,
		Wait:            true,
		Timeout:         5 * time.Minute,
		CreateNamespace: true,
		ValuesYaml: `
targetIngress: Nginx
`,
	}
	logger.Info(chartSpec.ChartName)
	logger.Info(chartSpec.Namespace)
	result := ctrl.Result{}

	if cr.DeletionTimestamp.IsZero() {
		if cr.Status.HelmchartInstalled == cr.Spec.HelmchartPath {
			if cr.Spec.Restart.Equal(&cr.Status.Restart) {

				return ctrl.Result{}, nil

			} else {
				if err := r.Client.Get(ctx, types.NamespacedName{Name: "jtarkanic-operator-helm-gin-server", Namespace: cr.Spec.Namespace}, &depl); err != nil {
					return ctrl.Result{}, err
				}

				if depl.Spec.Template.Annotations == nil {
					depl.Spec.Template.Annotations = make(map[string]string)
				}
				depl.Spec.Template.Annotations["restart"] = cr.Spec.Restart.GoString()
				err = r.Update(ctx, &depl)
				if err != nil {
					return ctrl.Result{}, err
				}
				cr.Status.Restart = cr.Spec.Restart
				if err := updateStatus(ctx, cr, patch, r); err != nil {
					return ctrl.Result{}, err
				}
				result = ctrl.Result{Requeue: true}

				return result, nil

			}

		} else {

			logger.Info("INSTALL")
			if err := installHelmChart(ctx, helmClient, chartSpec); err != nil {
				result = ctrl.Result{Requeue: true}
				return result, err
			}

			if err := addFinalizer(ctx, cr, patch, r); err != nil {
				return ctrl.Result{}, err
			}

			cr.Status.HelmchartInstalled = cr.Spec.HelmchartPath
			if err := updateStatus(ctx, cr, patch, r); err != nil {
				return ctrl.Result{}, err
			}

		}

	} else {

		logger.Info("UNINSTALL")
		if err := uninstallHelmChart(helmClient, chartSpec); err != nil {
			return ctrl.Result{}, err
		}

		if err := removeFinalizer(ctx, cr, patch, r); err != nil {
			return ctrl.Result{}, err
		}

	}

	return ctrl.Result{}, nil

}

// SetupWithManager sets up the controller with the Manager.
func (r *JtarkanicReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&goacademytsystemscomv1.Jtarkanic{}).
		WithEventFilter(predicate.GenerationChangedPredicate{}).
		Complete(r)
}

func installHelmChart(ctx context.Context, helmClient helmclient.Client, chartSpec helmclient.ChartSpec) error {

	// Install a chart release.
	// Note that helmclient.Options.Namespace should ideally match the namespace in chartSpec.Namespace.
	if _, err := helmClient.InstallOrUpgradeChart(ctx, &chartSpec, nil); err != nil {
		return err
	}

	return nil

}
func uninstallHelmChart(helmClient helmclient.Client, chartSpec helmclient.ChartSpec) error {

	if err := helmClient.UninstallRelease(&chartSpec); err != nil {
		return err
	}

	return nil
}
func updateStatus(ctx context.Context, cr goacademytsystemscomv1.Jtarkanic, patch client.Patch, r *JtarkanicReconciler) error {
	return r.Status().Patch(ctx, &cr, patch)
}

func addFinalizer(ctx context.Context, cr goacademytsystemscomv1.Jtarkanic, patch client.Patch, r *JtarkanicReconciler) error {

	if !controllerutil.ContainsFinalizer(&cr, myFinalizerName) {
		controllerutil.AddFinalizer(&cr, myFinalizerName)
		if err := r.Patch(ctx, &cr, patch); err != nil {
			return err
		}
	}

	return nil
}

func removeFinalizer(ctx context.Context, cr goacademytsystemscomv1.Jtarkanic, patch client.Patch, r *JtarkanicReconciler) error {

	if controllerutil.ContainsFinalizer(&cr, myFinalizerName) {

		controllerutil.RemoveFinalizer(&cr, myFinalizerName)
		err := r.Patch(ctx, &cr, patch)
		return err
	}

	return nil
}
