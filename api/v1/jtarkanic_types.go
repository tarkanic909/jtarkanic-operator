/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// JtarkanicSpec defines the desired state of Jtarkanic
type JtarkanicSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of Jtarkanic. Edit jtarkanic_types.go to remove/update
	HelmchartPath string      `json:"helmchartPath,omitempty"`
	Namespace     string      `json:"namespace"`
	Restart       metav1.Time `json:"restart,omitempty"`
}

// JtarkanicStatus defines the observed state of Jtarkanic
type JtarkanicStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Restart            metav1.Time `json:"restart,omitempty"`
	HelmchartInstalled string      `json:"helmchartIntalled"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Jtarkanic is the Schema for the jtarkanics API
type Jtarkanic struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   JtarkanicSpec   `json:"spec,omitempty"`
	Status JtarkanicStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// JtarkanicList contains a list of Jtarkanic
type JtarkanicList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Jtarkanic `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Jtarkanic{}, &JtarkanicList{})
}
