/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"errors"
	"fmt"
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var jtarkaniclog = logf.Log.WithName("jtarkanic-resource")

func (r *Jtarkanic) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!

//+kubebuilder:webhook:path=/mutate-goacademy-t-systems-com-v1-jtarkanic,mutating=true,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=jtarkanics,verbs=create;update,versions=v1,name=mjtarkanic.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &Jtarkanic{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *Jtarkanic) Default() {
	jtarkaniclog.Info("default", "name", r.Name)

	// TODO(user): fill in your defaulting logic.
	if r.Spec.HelmchartPath == "" {
		r.Spec.HelmchartPath = "/tmp/helm-gin-server-0.1.1.tgz"
	}
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-goacademy-t-systems-com-v1-jtarkanic,mutating=false,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=jtarkanics,verbs=create;update,versions=v1,name=vjtarkanic.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &Jtarkanic{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Jtarkanic) ValidateCreate() error {
	jtarkaniclog.Info("validate create", "name", r.Name)
	jtarkaniclog.Info("validate create", "helm chart path", r.Spec.HelmchartPath)

	// TODO(user): fill in your validation logic upon object creation.
	file, err := os.Stat(r.Spec.HelmchartPath)
	if err != nil {
		return errors.New("validation error")
	}
	if !file.Mode().IsRegular() {
		return fmt.Errorf("validation error")
	}

	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Jtarkanic) ValidateUpdate(old runtime.Object) error {
	jtarkaniclog.Info("validate update", "name", r.Name)

	// TODO(user): fill in your validation logic upon object update.
	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Jtarkanic) ValidateDelete() error {
	jtarkaniclog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
